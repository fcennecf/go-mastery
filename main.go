package main

import (
	"fmt"
	"gitlab.com/fcennecf/go-mastery/async"
)

func main() {
	//common.CommonExample()
	//common.TypesExample()
	//common.ControlStatementsExample()
	//common.FunctionsExamples()
	//structures.StructsExample()
	//async.ChannelsExample()
	//async.TimerExample()
	//async.TickerExample()
	//async.AfterFuncExample()
	//async.ContextExample()
	//async.PoolWorkersExample()
	async.ExampleWaiterGroup()
	fmt.Scanln()
}
