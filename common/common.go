package common

import (
	"fmt"
	"unicode/utf8"
)

const pi = 3.14 // Untyped const
const (
	_         = iota
	KB uint64 = 1 << (10 * iota)
	MB
)

type UserId int

func CommonExample() {
	fmt.Println("Hello world")

	var num0 int
	var num1 = 1
	num2 := num1 + 1

	fmt.Println(num0, num1, num2)

	text := "Hello"

	byteLen := len(text)
	symbols := utf8.RuneCountInString(text)
	fmt.Println(byteLen, symbols)

	var byteString = []byte(text)
	helloString := string(byteString)
	fmt.Println(helloString)

	fmt.Println("Constants", KB, MB)

	var uid UserId = 13
	myUid := UserId(1)
	fmt.Println(uid, myUid)

	// Pointers
	a := 1
	b := &a
	*b = 2
	c := &a
	fmt.Println(a, b, c)

	d := new(int)
	*d = 4
	*c = *d
	*d = 13

	fmt.Println(a, *c, *d, c, d)

	c = d
	*c = 14
	fmt.Println(a, *c, *d, c, d)

}
