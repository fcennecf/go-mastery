package common

import "fmt"

func TypesExample() {

	// Arrays
	const size = 2
	var m1 [2 * size]bool
	m2 := [...]int{1, 2}
	fmt.Println(m1, m2)

	// Slices
	buf := make([]int, 2, 8)
	buf2 := []int{1, 2}

	buf[1] = buf2[1]

	buf = append(buf, 13)
	fmt.Println(buf)

	var bufLen, bufCap = len(buf), cap(buf)
	fmt.Println(bufLen, bufCap)

	// Correct copy
	newBuf := make([]int, bufLen, bufCap)
	//copy(newBuf, buf)
	copy(newBuf[1:2], []int{2})
	fmt.Println(newBuf)

	//Map
	var user map[string]string = map[string]string{
		"name": "Raven",
	}
	// profiles := make(map[string]string, 4)
	fmt.Println(user["name"], len(user))

	delete(user, "name")
	name, isExist := user["name"]
	fmt.Println(name, isExist)

}
