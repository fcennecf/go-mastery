package structures

type Person struct {
	Id   int
	Name string
}

func (p *Person) SetName(name string) {
	p.Name = name
}

// p is immutable
func (p Person) UpdateName(name string) {
	p.Name = name
}
