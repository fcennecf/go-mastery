package structures

import "fmt"

type Registrant interface {
	Register(number int)
}

type Caller interface {
	Call(func(string) bool)
}

type CallerRegistrant interface {
	Caller
	Registrant
}

type Account struct {
	Id                 int
	RegistrationNumber int
	Cleaner            func(string) bool
	Owner              *Person
}

func (acc *Account) Register(number int) {
	acc.RegistrationNumber = number
}

func PrintInfo(r interface{}) {
	switch r.(type) {
	case *Account:
		account, ok := r.(*Account)
		if !ok {
			fmt.Printf("Convert error!")
		} else {
			fmt.Printf("Account %v\n", account.Owner.Name)
		}
	default:
		fmt.Println("Unknown Registrant")

	}

}
