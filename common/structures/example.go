package structures

import (
	"fmt"
)

func StructsExample() {
	person := Person{
		Id:   1,
		Name: "Alice",
	}

	account := Account{
		1,
		-1,
		nil,
		&person,
	}

	account.Register(1984)

	fmt.Printf("%T = %v \n", account, account)

	PrintInfo(&account)

	person.SetName("Mary")
	person.UpdateName("Mike")
	fmt.Println(person)
}
