package structures

import (
	"reflect"
	"testing"
)

func TestInterfaceRegistrantOk(t *testing.T) {
	expectedAccount := Account{
		1,
		1984,
		nil,
		nil,
	}

	account := Account{
		1,
		-1,
		nil,
		nil,
	}
	account.Register(1984)

	if !reflect.DeepEqual(account, expectedAccount) {
		t.Errorf("test for Registrant Interface Failed")
	}
}
