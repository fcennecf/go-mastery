package common

import "fmt"

func FunctionsExamples() {
	fmt.Println(notNamedResult())
	fmt.Println(multipleResult(1, 2, 3))
	fmt.Println(multipleResult(1, 2, 0))
	fmt.Println(notFixedParams(1, 3, 6))
	firstClassFunction()
	recoverPanicFunction()

}

func notNamedResult() string {
	name := "result"
	return name
}

func multipleResult(a, b int, c int) (result int, err error) {
	if c == 0 {
		err = fmt.Errorf("c is Zero")
		return
	}
	result = a * b * c
	return result, nil

}

func notFixedParams(p ...int) int {
	value := 0
	for _, val := range p {
		value += val
	}
	return value
}

func firstClassFunction() {

	func(in string) {
		fmt.Println(in)
	}("Mary")

	printer := func(val string) {
		fmt.Println(val)
	}
	printer("Mary")

	type callbackFunc func(string)
	func(callback callbackFunc) {
		callback("callback")
	}(printer)

}

func recoverPanicFunction() {
	deferTest()
}

func deferTest() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Happened error", err)
		}
	}()

	panic("Fatal error")
}
