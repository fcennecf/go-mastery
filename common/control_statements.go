package common

import "fmt"

func ControlStatementsExample() {
	data := map[string]string{}
	data["key"] = "value"

	if keyValue, isExist := data["key"]; isExist {
		fmt.Println("Kye is exist with", keyValue)
	}

	cond := 2
	if cond == 1 {
		// Never execute
	} else if cond == 2 {
		fmt.Println("If statment")
	}

	var swithcName string = "name"
	var switchAge = 20

	switch swithcName {
	case "name":
		fmt.Println("Got name")
		fallthrough
	case "lastName", "addName":
		fmt.Println("Got lastName")
	default:
		fmt.Println("Got default")
	}

	switch {
	case swithcName == "Vasa" || switchAge == 19:
		//
	case swithcName == "name" && switchAge >= 18:
		break
		fmt.Println("Bingo name 18")
	}

RidiculesLoop:
	for k, v := range data {
		fmt.Println("Got", k, v)
		switch {
		case k == "key":
			fmt.Println("Breake switch")
			break RidiculesLoop
		}
	}

	// Loops
	for {
		fmt.Println("Loop")
		break
	}
	isLoop := true
	for isLoop {
		fmt.Println("Loop")
		isLoop = false
	}

	for i := 0; i < 1; i++ {
		//if i == 0 {
		//	continue
		//}
		fmt.Println("Loop")
	}

	slice := []int{1, 2, 3}
	for idx, val := range slice {
		fmt.Println("Loop", idx, val)
	}

}
