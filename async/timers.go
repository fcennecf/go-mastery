package async

import (
	"fmt"
	"time"
)

func longQuery() chan bool {
	ch := make(chan bool, 1)
	go func() {
		time.Sleep(2 * time.Second)
		ch <- true
	}()
	return ch
}

func TickerExample() {
	ticker := time.NewTicker(time.Second)
	i := 0
	for tick := range ticker.C {
		i++
		fmt.Println("step", i, "time", tick)
		if i >= 5 {
			ticker.Stop()
			break
		}
	}
	// Alias for NewTicker return chanel
	//c := time.Tick(time.Second) i=0
	//for tickTime := range c
	fmt.Println("total", i)
}

func sayHello() {
	fmt.Println("Hello World")
}

func AfterFuncExample() {
	timer := time.AfterFunc(time.Second*2, sayHello)
	fmt.Scanln()
	timer.Stop()
}

func TimerExample() {
	timer := time.NewTimer(1 * time.Second)
	select {
	case <-timer.C:
		fmt.Println("timer.C timeout happend")
	case <-time.After(time.Minute):
		fmt.Println("time.After timeout happend")
	case result := <-longQuery():
		if !timer.Stop() {
			//Clear resource
			<-timer.C
		}
		fmt.Println("operation result:", result)
	}
}
