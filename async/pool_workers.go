package async

import (
	"fmt"
	"runtime"
	"time"
)

const goroutinesPoolNum = 3

func workerStart(workerNum int, in <-chan string) {
	for input := range in {
		fmt.Println(workerNum, input)

		runtime.Gosched() // попробуйте закомментировать
	}
}

func PoolWorkersExample() {
	workerInput := make(chan string, 2)
	for i := 0; i < goroutinesPoolNum; i++ {
		go workerStart(i, workerInput)
	}
	months := []string{"Январь", "Февраль", "Март",
		"Апрель", "Май", "Июнь",
		"Июль", "Август", "Сентябрь",
		"Октябрь", "Ноябрь", "Декабрь",
	}
	for _, monthName := range months {
		workerInput <- monthName
	}
	close(workerInput)

	time.Sleep(time.Millisecond)
}
