package async

import "fmt"

func firstExample() {
	ch1 := make(chan int, 0)
	go func(in chan int) {
		val := <-in
		fmt.Println("GO: Read from chanel", val)
		fmt.Println("GO: after read from chan")
	}(ch1)

	ch1 <- 1984
	fmt.Println("MAIN: after put to chan")
}

func secondExample() {
	ch1 := make(chan int, 0)
	go func(out chan<- int) {
		for i := 0; i < 5; i++ {
			fmt.Println("before", i)
			out <- i
			fmt.Println("after", i)
		}

		close(ch1)
		fmt.Println("Generate finish")
	}(ch1)

	for i := range ch1 {
		fmt.Println("\tget", i)
	}

}

func multiplexExample() {
	ch1 := make(chan int, 2)
	ch1 <- 1
	ch1 <- 2
	ch2 := make(chan int, 2)
	ch2 <- 3

	for {
		select {
		case val := <-ch1:
			fmt.Println("get val from ch1", val)
		//case ch2 <- 1:
		//	fmt.Println("put val to ch2")
		case val := <-ch2:
			fmt.Println("get val from ch2", val)
		default:
			fmt.Println("default case")
			return
		}
	}
}

func cancelChanelExample() {
	cancleCh := make(chan struct{})
	dataCh := make(chan int)

	go func(cancleCh <-chan struct{}, dataCh chan<- int) {
		val := 0
		for {
			select {
			case <-cancleCh:
				return
			case dataCh <- val:
				val++
			}
		}
	}(cancleCh, dataCh)

	for curVal := range dataCh {
		fmt.Println("read", curVal)
		if curVal > 3 {
			fmt.Println("send cancel")
			cancleCh <- struct{}{}
			break
		}
	}

}

func ChannelsExample() {
	//firstExample()
	//secondExample()
	multiplexExample()
	//cancelChanelExample()
}
