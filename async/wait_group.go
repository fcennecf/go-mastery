package async

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

const (
	iterationsGroupNum = 7
	goroutinesGroupNum = 5
)

func startWorker(in int, wg *sync.WaitGroup) {
	defer wg.Done()
	for j := 0; j < iterationsGroupNum; j++ {
		fmt.Printf(formatWork(in, j))

		runtime.Gosched()
	}
}

func ExampleWaiterGroup() {
	wg := &sync.WaitGroup{}
	for i := 0; i < goroutinesGroupNum; i++ {
		wg.Add(1)
		go startWorker(i, wg)
	}
	time.Sleep(time.Millisecond)
	wg.Wait()
}
