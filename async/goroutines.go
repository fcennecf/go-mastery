package async

import (
	"fmt"
	"runtime"
	"strings"
)

const (
	iterationsNum = 7
	goroutinesNum = 5
)

func doSomeWork(in int) {
	for i := 0; i < iterationsNum; i++ {
		fmt.Println(formatWork(in, i))
		runtime.Gosched()
	}
}

func formatWork(in, i int) string {
	return fmt.Sprintln(strings.Repeat(" ", in), "*",
		strings.Repeat(" ", goroutinesNum-in), "th", in,
		"iter", i, strings.Repeat("*", i))
}

func SimpleGoroutinesExample() {
	for i := 0; i < goroutinesNum; i++ {
		go doSomeWork(i)
	}
}
